require 'nokogiri'

class ProductParser
  attr_reader :page

  def initialize(html:)
    @html = html
    @page =  Nokogiri::HTML.parse(html)
  end

  # Parse the amazon product page for the description of the product
  # @return [string] product description
  def description
    description_text = page.css('div[id="productDescription"]').text
    sanitize_text_to_array(description_text).join(' ')
  end

  # Parse the amazon product page for product cattegories
  # @return [array] product cattegories
  def categories
    category_text = page.css('a[class="a-link-normal a-color-tertiary"]').text
    sanitize_text_to_array(category_text)
  end

  # Parse the amazon product page for the title/name of the product
  # @return [string] product title
  def name
    page.title
  end

  # Parse the amazon product page's tables for the rank element
  # @return [string, nil] product rank
  def rank
    rank_text = 'Sellers Rank'
    text = fetch_info_table_data('td', rank_text) || 
           fetch_info_table_data('th', rank_text) ||
           fetch_info_table_data('th', 'Rank') ||
           fetch_info_table_data('td', 'Rank')

    return unless text
    text.gsub("\n", '').split('#').map(&:lstrip).map(&:rstrip).select {|str| !str.empty? }.join('. ')
  end

  # Parse the amazon product page's tables for the dimensions element
  # @return [string, nil] product dimensions
  def dimensions
    dimension_text = 'Product Dimensions'
    text = fetch_info_table_data('td', dimension_text) || 
           fetch_info_table_data('th', dimension_text) ||
           fetch_info_table_data('th', 'Dimensions') ||
           fetch_info_table_data('td', 'Dimensions')

    return unless text
    text.lstrip.rstrip
  end

  # Parse the amazon product page for the rating element
  # @return [string, nil] product rating
  def rating
    page.css('//div[id="reviewSummary"]//span[class="a-icon-alt"]').first&.text
  end

  private

  # Checks for the presece of table data based on the value of adjacent td or th elements
  # @param [string] adjacent_element the type of table element adjacent to the data being fetched
  # @param [string] adjacent_element_text the string of the adjacent_element you are trying to match on
  # @return [string] string of the elemet immediately following the adjacent_element
  def fetch_info_table_data(adjacent_element, adjacent_element_text)
    adjacent_elements = page.xpath("//#{adjacent_element}[contains(text(), '#{adjacent_element_text}')]")
    if adjacent_elements.any?
      return adjacent_elements.first.next_element.text
    end
  end

  # Strips out unnecessary newline characters and white space.
  # @param [string] text the text you want sanitized
  # @return [array] array of sanitized text, split on new line characters
  def sanitize_text_to_array(text)
    text.split("\n").map(&:lstrip).map(&:rstrip).select { |c| !c.empty? }
  end
end