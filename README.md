### ASIN Search API

#### What is ASIN?

The Amazon Standard Identification Number (ASIN) is a 10-character alphanumeric unique identifier assigned by Amazon and its partners for product identification within the Amazon organization.

#### Description
This is an exercise to create a simple amazon product parser that fetches Amazon product details based on an ASIN. 

The app is built in Sinatra, with a Postgres database connected via Active Record.

The chosen solution here was to take an ASIN, use it to fetch the product page and parse the HTML returned.

Parsing comes with its own challenges:
* Amazon's product pages vary significantly from product to product, meaning you would need a lot of samples to have any effective data parsing strategy - especially for data like ranks and dimensions, which can be found in a number of locations.
* Parsing is not a long term solution if the product pages can be redesigned and changed
* This requires you making a call to and parsing the product page, which is large, meaning it is relatively slow.

#### Preferred solution

A better solution to fetching Amazon Product Data would be to use an API specialised to do so. Amazon offers the `Product Advertising API` that would make this challenge much easier, however getting access is a challenge.

#### Setup

* Ensure you have `ruby` set up on your machine. https://www.ruby-lang.org/en/documentation/installation/
* Ensure you have `Postgres` set up on your machine.
* Copy over the example YAML files found in `/config` and update DB credentials as needed.
* Create your DB `createdb asin_search`.
* Ensure you have `bundler` installed:  `gem install bundler`.
* Install all dependencies using `bundle install`.
* Use `rackup -p 9000` to start up the server.
* Content will now be served on `http://localhost:9000/`.

#### API

* To search for the product make a `GET` call to `http://localhost:9000/proucts/:asin` where `:asin` is the ASIN of the product you are looking for.
* Expect a `404` if the ASIN is invalid.
* Expect the following JSON structure if the ASIN is valid:
```
{
  "id": "SOME UUID,
  "asin": "ASIN",
  "categories":["CAT1", "CAT2"],
  "description": "FOO",
  "dimensions": "1X1",
  "name": "FOO",
  "rank": "#1 in FOO",
  "rating": "5 out of 5",
  "pulled_at": "2018-06-15T13:42:02.556Z",
  "created_at": "2018-06-15T13:42:02.556Z",
  "updated_at": "2018-06-15T13:42:02.556Z"
}
```