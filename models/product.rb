class Product < ActiveRecord::Base
  validates_presence_of :asin, :name
end