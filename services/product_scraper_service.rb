require 'rest-client'
require_relative '../app/product_parser'
require_relative '../models/product'

class ProductScraperService
  BASE_URL =  'https://www.amazon.com/dp/'.freeze
  HEADERS = {
    'User-Agent'=> 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'
  }.freeze

  def initialize(asin:)
    @asin = asin
  end

  # Makes a call to the amazon page with @asin and parses the returned html if the call is succesful.
  # Takes the result of the parser and created a new Product record. The RestClient will raise an error
  # if unsucessful.
  # @return [Product, ] Instance of the product record created
  def perform
    url = BASE_URL + @asin
    request = RestClient.get(url, HEADERS)
    parser = ProductParser.new(html: request.body)
    
    Product.create(
      asin: @asin,
      categories: parser.categories,
      description: parser.description,
      dimensions: parser.dimensions,
      name: parser.name || 'no name found',
      rank: parser.rank,
      rating: parser.rating,
      pulled_at: Time.now
    )
  end
end