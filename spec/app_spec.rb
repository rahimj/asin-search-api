require 'rack/test'
require 'rest-client'
require 'webmock/rspec'
require_relative '../app'
require_relative '../models/product'
require_relative '../services/product_scraper_service'


describe ASINSearch do
  WebMock.disable_net_connect!
  include Rack::Test::Methods

  def app
    described_class
  end

  describe 'GET /' do
    it 'responds with 200 and a success status' do
      get '/'

      expect(last_response.body).to eq('ASIN Search FTW')
      expect(last_response.status).to eq(200)
    end
  end

  describe '/products/:asin' do
    let(:product) { Product.new(asin: 'asin', name: 'name') }
    let(:current_path) { '/products/asin' }

    context 'with a Product in the DB' do
      it 'responds with 200 and a serialised JSON' do
        expect(Product).to receive(:find_by).with(asin: 'asin').and_return(product)
        get current_path
        expect(last_response.body).to eq(product.to_json)
        expect(last_response.status).to eq(200)
      end
    end

    context 'without a product in the DB' do
      before do
        allow_any_instance_of(ProductScraperService).to receive(:perform).and_return(product)
      end
      it 'responds with 200 and a serialised JSON' do
        get current_path
        expect(last_response.body).to eq(product.to_json)
        expect(last_response.status).to eq(200)
      end
    end

    context 'with an error fetching a product' do
      before do
        allow_any_instance_of(ProductScraperService).to receive(:perform).and_raise(RestClient::ExceptionWithResponse)
      end
      it 'responds with 404 and an error' do
        get current_path
        expect(last_response.body).to eq({ error: "Invalid ASIN" }.to_json)
        expect(last_response.status).to eq(404)
      end
    end
  end
end