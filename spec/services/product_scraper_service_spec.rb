require 'rest-client'
require 'webmock/rspec'
require_relative '../../services/product_scraper_service'
require_relative '../../app/product_parser'
require_relative '../../models/product'

WebMock.disable_net_connect!

describe ProductScraperService do
  let(:asin) { 'some_asin' }
  let(:subject) { described_class.new(asin: asin) }
  let(:parser) { 
    double(
      'parser', 
      categories: ['categories'],
      description: 'description',
      dimensions: 'dimensions',
      name: 'name',
      rank: 'rank',
      rating: 'rating'
    )
  }
  let(:product) { Product.new(asin: 'asin', name: 'name') }
  let(:rest_client_response) {double('body', body: '<html/>')}
  before do 
    allow(ProductParser).to receive(:new).and_return(parser)
    allow(RestClient).to receive(:get).and_return(rest_client_response)
  end
  
  context '' do
    it 'creates a new product' do
      expect(Product).to receive(:create).with(
        asin: asin,
        categories: parser.categories,
        description: parser.description,
        dimensions: parser.dimensions,
        name: parser.name,
        rank: parser.rank,
        rating: parser.rating 
      )

     subject.perform
    end
  end
end
