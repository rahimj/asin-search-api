require_relative '../../app/product_parser'

describe ProductParser do
  let(:html) { File.read(File.join(__dir__, '../fixtures/product1.html')) }
  let(:subject) { described_class.new(html: html) }

  describe '#description' do
    specify{expect(subject.description).to include 'Food Grade silicone teething'}
  end
  describe '#categories' do
    specify{expect(subject.categories).to include 'Baby Care'}
  end
  describe '#name' do
    specify{expect(subject.name).to include 'Baby Banana'}
  end
  describe '#rank' do
    specify{expect(subject.rank).to include '10 in Baby'}
  end
  describe '#dimensions' do
    specify{expect(subject.dimensions).to eq '4.3 x 0.4 x 7.9 inches'}
  end
  describe '#rating  ' do
    specify{expect(subject.rating).to include '4.7 out of 5 stars'}
  end
end