class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products, id: :uuid do |t|
      t.string :asin
      t.string :categories, array: true, default: []
      t.string :description
      t.string :dimensions
      t.string :name
      t.string :rank
      t.string :rating
      t.datetime :pulled_at, default: nil

      t.timestamps null: false
    end
    add_index :products, :asin
  end
end
