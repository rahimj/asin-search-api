require 'sinatra'
require 'sinatra/config_file'
require 'sinatra/activerecord'
require 'rest-client'
require 'nokogiri'
require_relative './models/product'
require_relative './services/product_scraper_service'


class ASINSearch < Sinatra::Base
  register Sinatra::ConfigFile
  register Sinatra::ActiveRecordExtension
  
  config_file 'config/application.yml'

  set :root, File.dirname(__FILE__)
  set :port, settings.port
  set :show_exceptions, false

  before do
    headers["Access-Control-Allow-Origin"] = settings.client_url
    headers["Access-Control-Allow-Methods"] = "GET, HEAD, POST, PUT"
    headers["Access-Control-Allow-Headers"] = "Content-Type, Accept, Origin, Authorization"
    headers["Access-Control-Max-Age"] = "86400"
    content_type :json
  end

  error RestClient::ExceptionWithResponse do 
    [404, { error: "Invalid ASIN" }.to_json]
  end

  error do
    "OOPS! Something went wrong"
  end

  get '/' do
    'ASIN Search FTW'
  end

  get '/products/:asin' do
    asin = params[:asin]
    product = Product.find_by(asin: asin)
    return product.to_json if product
    scraper = ProductScraperService.new(asin: asin)
    product = scraper.perform
    product.to_json
  end
end